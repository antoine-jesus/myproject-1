package net.tncy.jesus.myproject1;

public class Calculateur {

    public double division(int a, int b) {
        double aa = (double) a;
        double bb = (double) b;
        return b == 0 ? 0.0 : aa / bb;
    }
}
