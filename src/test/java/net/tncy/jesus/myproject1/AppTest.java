package net.tncy.jesus.myproject1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void divisionTest() {
        Calculateur c = new Calculateur();
        double delta = 1e-15;
        assertEquals(c.division(2, 2), 1.0, delta);
        assertEquals(c.division(5, 2), 2.5, delta);
        assertEquals(c.division(2, -2), -1.0, delta);
        assertEquals(c.division(2, 0), 0.0, delta);
    }
}
